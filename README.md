# ft_malloc

| Branch    | Build status    |
| :-------- | :-------------: |
| master    | [![Build Status](https://travis-ci.org/tnicolas42/ft_malloc.svg?branch=master)](https://travis-ci.org/tnicolas42/ft_malloc) |
| develop   | [![Build Status](https://travis-ci.org/tnicolas42/ft_malloc.svg?branch=develop)](https://travis-ci.org/tnicolas42/ft_malloc) |
